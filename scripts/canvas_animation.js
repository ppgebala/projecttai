var canvas
var context;
var x = 100;
var y = 20;
var dy = 10;
var kolory = new Array("blue", "red", "yellow", "green");
var i = 0;

function draw() {
	canvas = document.getElementById('animacja_1');
	context = canvas.getContext('2d');
	setInterval(anim, 100);
}

function anim() {
	context.clearRect(0, 0, canvas.width, canvas.height);
	y += dy;
	if (y > (canvas.height - 30)) {
		dy = -10;
		i = ++i % kolory.length;
	}
	if (y < 30) {
		dy = 10;
		i = ++i % kolory.length;
	}
	context.beginPath();
	context.fillStyle = kolory[i];
	context.arc(x, y, 20, 0, Math.PI * 2, true);
	context.closePath;
	context.fill();
}