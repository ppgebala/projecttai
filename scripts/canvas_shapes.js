function draw()	{
		var c_line = document.getElementById("line");
		var context = c_line.getContext("2d");
		context.moveTo(0,0);
		context.lineTo(200,100);
		context.stroke();
		
		var c_circle = document.getElementById("circle");
		var context = c_circle.getContext("2d");
		context.beginPath();
		context.arc(95,50,40,0,2*Math.PI);
		context.stroke();
		
		var c_text = document.getElementById("text");
		var context = c_text.getContext("2d");
		context.font = "32px Arial";
		context.fillText("Technologie Aplikacji Internetowych",10,50);
		
		var c_stext = document.getElementById("s_text");
		var context = c_stext.getContext("2d");
		context.font = "32px Arial";
		context.strokeText("Technologie Aplikacji Internetowych",10,50);
		
		var c_gradient = document.getElementById("gradient");
		var context = c_gradient.getContext("2d");

		var grd = context.createLinearGradient(0,0,200,0);
		grd.addColorStop(0,"red");
		grd.addColorStop(1,"white");

		context.fillStyle = grd;
		context.fillRect(10,10,150,80);
		
		var c_bazier = document.getElementById("bazier");
		var context = c_bazier.getContext("2d");
		context.beginPath();
		context.bezierCurveTo(0, 50, 250, 50, 300, 200);
		context.closePath();
		context.stroke();
	}