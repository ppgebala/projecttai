function draw()	{
		var c_html_back = document.getElementById("html_back");
		var context = c_html_back.getContext("2d");
		
		context.shadowOffsetX = 10;
		context.shadowOffsetY = 10;
		context.shadowBlur = 4;
		context.shadowColor = 'rgba(0, 0, 0, 0.5)';
		context.beginPath();
		
		context.beginPath();
		context.moveTo(10,10);
		context.lineTo(40,250);
		context.lineTo(120,270);
		context.lineTo(200,250);
		context.lineTo(230,10);
		context.closePath();
		context.fillStyle = 'rgb(199,80,51)';
		context.fill();

		context.beginPath();
		context.moveTo(120,35);
		context.lineTo(120,245);
		context.lineTo(185,230);
		context.lineTo(205,35);
		context.closePath();
		context.fillStyle = 'rgb(203,97,57)';
		context.fill();
		
		context.beginPath();
		context.moveTo(190,60);
		context.lineTo(50,60);
		context.lineTo(60,140);
		context.lineTo(150,140);
		context.lineTo(145,190);
		context.lineTo(120,200);
		context.lineTo(95,190);
		context.lineTo(90,160);
		context.lineTo(65,160);
		context.lineTo(75,210);
		context.lineTo(120,225);
		context.lineTo(170,210);
		context.lineTo(180,115);
		context.lineTo(80,115);
		context.lineTo(77,85);
		context.lineTo(185,85);
		context.closePath();
		context.fillStyle = 'rgba(255,255,255,0.95)';
		context.fill();
		
		context.beginPath();
		context.moveTo(315,120);
		context.lineTo(315,140);
		context.lineTo(295,140);
		context.lineTo(295,160);
		context.lineTo(315,160);
		context.lineTo(315,180);
		context.lineTo(335,180);
		context.lineTo(335,160);
		context.lineTo(355,160);
		context.lineTo(355,140);
		context.lineTo(335,140);
		context.lineTo(335,120);
		context.closePath();
		context.fillStyle = 'rgb(156,156,156)';
		context.fill();
		
		context.beginPath();
		context.moveTo(410,10);
		context.lineTo(440,250);
		context.lineTo(520,270);
		context.lineTo(600,250);
		context.lineTo(630,10);
		context.closePath();
		context.fillStyle = 'rgb(19,108,168)';
		context.fill();
		
		
		context.beginPath();
		context.moveTo(520,35);
		context.lineTo(520,245);
		context.lineTo(585,230);
		context.lineTo(605,35);
		context.closePath();
		context.fillStyle = 'rgb(47,155,204)';
		context.fill();
		
		context.beginPath();
		context.moveTo(590,60);
		context.lineTo(450,60);
		context.lineTo(453,85);
		context.lineTo(525,85);
		context.lineTo(457,115);
		context.lineTo(460,140);
		context.lineTo(550,140);
		context.lineTo(545,190);
		context.lineTo(520,200);
		context.lineTo(495,190);
		context.lineTo(490,160);
		context.lineTo(465,160);
		context.lineTo(475,210);
		context.lineTo(520,225);
		context.lineTo(570,210);
		context.lineTo(580,115);
		context.lineTo(515,115);
		context.lineTo(585,85);
		context.closePath();
		context.fillStyle = 'rgba(255,255,255,0.95)';
		context.fill();
	}