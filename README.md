# Projekt na przedmiot "Technologie Aplikacji Internetowych" #

## Cel projektu ##

Celem projektu jest stworzenie witryny internetowej bazującej na technologiach HTML5
oraz CSS3. Projekt ma na celu zaprezentowanie zaawansowanych wzorców projektowych wyżej
wymienionych technik i powinien zawierać elementy takie jak:
* rysowanie na kanwach,
* osadzanie danych audio i wideo,
* tworzenie formularzy,
* tworzenie „ulepszonych” interfejsów użytkownika z użyciem CSS3,
* selektory i dziedziczenie w CSS3,
* efekty przejścia,
* modele polowe oraz ich własności,
* modele pozycjonowania.

## Lista wymagań ##

* łatwy dostęp do szukanych przez użytkownika treści,
* przejrzysty wygląd i jasny układ informacji,
* spójność poszczególnych podstron serwisu,
* prosty i intuicyjny system nawigacji,
* możliwość zapisu dużych bloków tekstu do pliku,
* strona główna dostarcza konkretnych informacji na temat witryny, opis treści jakie
zawiera,
* strona główna posiada dobrze wyeksponowaną nazwę i logo,
* strona główna powinna zachęcać użytkownika do odwiedzenia witryny,
* strona wczytuje się w czasie nie większym niż 10 sekund,
* rozmiar strony nie powinien przekraczać 40 KB.

## Interfejs graficzny ##

![Przechwytywanie.PNG](https://bitbucket.org/repo/gX9Gep/images/53917682-Przechwytywanie.PNG)